# Sample Node.js Application

Receives a POST payload and print it in the console

## Deploy it on Openshift
  oc new-app  https://gitlab.com/openshift-samples/node-log-text.git -l template=node-log-text

## Clean Created Objects
 oc delete all -l template=node-log-text
